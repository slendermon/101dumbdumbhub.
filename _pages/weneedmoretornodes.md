---
title: "We Need More tor Nodes"
category: "Privacy &amp; Security"
type: post 
status: publish 
permalink: /we-need-more-tor-nodes
author: Mr T
date: "2022-05-24"
thumbnail: ../uploads/2022/05/download-1.png
---

We Need More tor Nodes 
=====
Tor only currently has 6000 relays and 2000 bridges. 
Who knows how many of them can easily be made by the government agencies. 
Tor is our defense towards censorship. And we can start making more relays and 
bridges to help decentralize the internet.
