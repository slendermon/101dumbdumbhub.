---
permalink: /
title: "Blog"
excerpt: "i know you're using windows and chrome"
author_profile: false
redirect_from: 
  - /about/
  - /about.html
---
  
[I know you're using Windows and Chrome](https://randomsource.club/i-know-youre-using-windows-and-chrome)
======
Posted on by Mr T\
<img width=200 class="center" src="https://searx.tiekoetter.com/image_proxy?url=https%3A%2F%2Fwww.windowslatest.com%2Fwp-content%2Fuploads%2F2021%2F10%2FChrome-for-Windows-11.jpg&amp;h=f93819310cceea7b02ab0c84af1a609fa6f32908bd63e0b11ed1c5c56cb23679" alt="Here&#39;s our first look at Google Chrome&#39;s new design for Windows 11"/>\
Stop it. Use BSD or linux. Because I can see what you are doing on your computer.\
Posted in Computing

[More people need websites.](https://randomsource.club/more-people-need-websites) 
======
Posted on by Mr T\
<img width=200 src="https://searx.tiekoetter.com/image_proxy?url=https%3A%2F%2Fs1.qwant.com%2Fthumbr%2F474x335%2F1%2F9%2F2fd439b70bd623745db1436a81ec46f1455d41e0664418892c8f045541291a%2Fth.jpg%3Fu%3Dhttps%253A%252F%252Ftse1.mm.bing.net%252Fth%253Fid%253DOIP.onSt3CPfaMKTUYnY56yHFgHaFP%2526pid%253DApi%26q%3D0%26b%3D1%26p%3D0%26a%3D0&amp;h=555dd9a9db14867bb0f9c56226f0c18ee7302c98c45417dde5f07e67e3cb9b18" alt="Build Websites: 3 Major Workflows in 2015 (infographic)"/>\
If you're interested in computers, like me, I think it is actually really fun setting up your own website.\
Posted in Uncategorized

[What is Monero, and why do I recommend it?](https://randomsource.club/what-is-monero-and-why-do-i-recommend-it)
======
Posted on by Mr T\
<img width=200 class="center" src="https://searx.tiekoetter.com/image_proxy?url=https%3A%2F%2Fwww.getmonero.org%2Fpress-kit%2Fsymbols%2Fmonero-symbol-on-white-480.png&amp;h=0d302fda652987aa0b8f8f901a0fe651014abca62498005533eac710295c57a6" alt="Home | Monero - secure, private, untraceable"/>\
If you've been interested about crypto, you may or may not know about the big name bitcoin. Bitcoin has been made as an alternate way to pay digitally "privaately" and "securely". Unfortunately, through the paces of time it hasn't stood up to its claims. Bitcoin can be traced and can be seen by how it [...]\
Posted in Privacy & Security

[We Need More tor Nodes](https://randomsource.club/we-need-more-tor-nodes)
======
Posted on by Ben R\
<img width=200 class="center" src="https://searx.tiekoetter.com/image_proxy?url=https%3A%2F%2Fwww.welivesecurity.com%2Fwp-content%2Fuploads%2F2014%2F10%2Ftor-onion.jpg&amp;h=4fcdf7f31b5239bd13d5b21292b170076db423ecfac76307af4402a894f54f43" alt="Tor users targeted with exit node malware | WeLiveSecurity"/>\
Tor onl/y currently has 6000 relays and 2000 bridges. Who knows how many of them can easily be made by the government agencies. Tor is our defense towards censorship. And we can start making more relays and bridges to help decentralize the internet.\ 
Posted in Privacy & Security 

[What is solar? And why does it matter?](https://randomsource.club/what-is-solar-and-why-does-it-matter?)
======
Posted on by Ben R\
<img width=200 class="center" src="https://searx.tiekoetter.com/image_proxy?url=https%3A%2F%2Fs1.qwant.com%2Fthumbr%2F474x395%2F8%2F1%2F3526de78a6d0684353a5c0d11629c01cd1f58614d7874ae1c511af560514db%2Fth.jpg%3Fu%3Dhttps%253A%252F%252Ftse2.mm.bing.net%252Fth%253Fid%253DOIP.UW9c9PKjIisZVXJDIsSZ6wHaGL%2526pid%253DApi%26q%3D0%26b%3D1%26p%3D0%26a%3D0&amp;h=cb7da3dda5c3f97f7032bc187519e625b79fc65e60048e2855135380479da6ee" alt="6.3 KiloWatt Ground Mount Home Solar Array : 20 Steps (with Pictures ..."/>\
There are many ways to get electricity, especially from sources of coal and nonrenewable resources. But who knows if we could ever get out of this debt of ever ending energy, that we will one day have to rely on renewable sources, like solar and hydroelectric and wind. Solar is the most budget conscious friendly [...]\
Posted in Green Tech 

[Advice for myself on Web hosting](https://randomsource.club/advice-for-myself-on-web-hosting) 
======
Posted on by Mr T\
<img width=200 class="center" src="https://searx.tiekoetter.com/image_proxy?url=https%3A%2F%2Fs2.qwant.com%2Fthumbr%2F474x324%2F5%2F3%2F20e316b9b08675cae5bf12ed412ab590622bc7db20d19a4bc5e5356efd5351%2Fth.jpg%3Fu%3Dhttps%253A%252F%252Ftse1.mm.bing.net%252Fth%253Fid%253DOIP.FBl0Mf6DXgBPPlm6iMvoKwHaFE%2526pid%253DApi%26q%3D0%26b%3D1%26p%3D0%26a%3D0&amp;h=f91ec9ed370bf1e504ca98746e507afe26cf8573c527ef7003605c36ea4b68f0" alt="Major Advantages Of Utilizing Dedicated Hosting Server"/>\
I'm thinking one day I might make 'servers' locally rather than using a VPS or web hosting. But the reason why I haven't done that, is because I don't have an ISP that allows me to host my own email/website. Sorry this was a very short blog post. Maybe I should combine this with my [...]\
Posted in Web Hosting

[How I Made This Website](https://randomsource.club/how-i-made-this-website/)
======
Posted on by Mr T\
<img width=200 class="center" src="https://searx.tiekoetter.com/image_proxy?url=https%3A%2F%2Fs2.qwant.com%2Fthumbr%2F474x266%2F5%2F5%2Ff60b932d16d00b0a11ee120b10676049e713328fb6e9f8e6dbfe28f6086c52%2Fth.jpg%3Fu%3Dhttps%253A%252F%252Ftse3.mm.bing.net%252Fth%253Fid%253DOIP.X9hICRuMInrLJz9Y8JtaQwHaEK%2526pid%253DApi%26q%3D0%26b%3D1%26p%3D0%26a%3D0&amp;h=09b251198f2950c8f83f400cc49a5555e4e93ac1f905300510d0c04fba85343e" alt="Important Points You Need to Consider About Web Hosting"/>\
This is my first post about websites. How did I make this website? I use Monero signing up for a domain service and a web hosting & VPS service. Monero is a sort of currency I recommend for privacy and security, because it is made to emulate cash but digitally. I use Njalla, and 1984, [...]\
Posted in Web Hosting \
Leave a Comment

[Archives](https://randomsource.club/archives) [Categories](https://randomsource.club/categories)\
[May 2022](https://randomsource.club/may-2022) [Computing](https://randomsource.club/computing)\
                      [Green Tech](https://randomsource.club/green-tech)\
                      [Privacy & Security](https://randomsource.club/privacy-&-security)\
                      [Uncategorized](https://randomsource.club/uncategorized)\
                      [Web Hosting](https://randomsource.club/web-hosting) 
