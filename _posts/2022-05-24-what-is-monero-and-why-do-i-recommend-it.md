---
title: "What is Monero, and why do I recommend it?"
collection: Privacy & Security
type: "Privacy &amp; Security"
permalink: /what-is-monero-and-why-do-i-recommend-it
author: Mr T
date: "2022-05-24"
thumbnail: ../uploads/2022/05/image_proxy.png
---

What is Monero, and why do I recommend it?
======
If you’ve been interested about crypto, you may or may not know 
about the big name bitcoin. Bitcoin has been made as an alternate 
way to pay digitally “privately” and “securely”. Unfortunately, through the 
paces of time it hasn’t stood up to its claims. Bitcoin can be traced and can be 
seen by how it has been used in the past for purchases.

This is where Monero comes in. Monero was made by a group of crypto enthusiasts 
making a private, secure and decentralized way of paying online. If there is new 
Monero put in a crypto wallet, it will have two statements, one “locked” and another 
“unlocked”. Locked isn’t able to be used until it is done reading the blocks. Once it 
is done, it will be read as unlocked and you may pay with it. But once you pay, it will 
take some time to be used again. Monero has a blockchain technology that allows people to 
use it anywhere else, and it isn’t linking to personally identifiable information about you. 
It uses a decentralized blockchain, that means that the crypto mixes with others and 
doesn’t tie your payment to your purchases.
