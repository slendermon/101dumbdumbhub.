---
title: 'How I Made This Website'
date: '2022-05-04T21:43:17+00:00'
status: publish
permalink: /how-i-made-this-website
author: 'Mr T'
excerpt: ''
type: post
id: 1
thumbnail: ../uploads/2022/05/download-1.jpeg
category:
    - 'Web Hosting'
tag: []
post_format: []
---

<a href="{{ base_path }}/">This is my first post about websites.</a>
======

How did I make this website? I use Monero signing up for a domain service and a 
web hosting &amp; VPS service. Monero is a sort of currency I recommend for privacy 
and security, because it is made to emulate cash but digitally. I use Njalla, and 1984, 
to create this website.

For Njalla, I used it for domain registration:

I made one ‘a’ DNS and connected it via www (just www).

Then, I make one ‘a’ DNS again and just use @ sign (for root).

For 1984, I used it for VPS hosting and web hosting:

Just sign up via web hosting (if ‘private’, you need monero, otherwise it won’t let you) 
and setup a CMS. A CMS is a content management system. 1984 comes with one preinstalled 
for web hosts.
