---
title: 'What is solar? And why does it matter?'
date: '2022-05-13T21:01:00+00:00'
status: publish
permalink: /what-is-solar-and-why-does-it-matter
author: 'Ben R'
excerpt: ''
type: post
id: 25
thumbnail: ../uploads/2022/05/Untitledsolargmipa.png
category:
    - 'Green Tech'
tag: []
post_format: []
---
There are many ways to get electricity, especially 
from sources of coal and nonrenewable resources. But who knows if we 
could ever get out of this debt of ever ending energy, that we will one day have to 
rely on renewable sources, like solar and hydroelectric and wind. Solar is the most 
budget conscious friendly out of all. And where will all of this electricity go to if we 
hog all of the resources anyway. Over using water and over using materials just to make 
renewable energy.
